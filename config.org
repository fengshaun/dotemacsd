* Packages
** =MELPA=, =ELPA=, and =org-mode=

make sure the basics are setup including =use-package=.

#+BEGIN_SRC emacs-lisp
  (require 'package)

  (add-to-list 'package-archives '("gnu" . "https://elpa.gnu.org/packages/"))
  (add-to-list 'package-archives '("melpa-stable" . "https://stable.melpa.org/packages/"))
  (add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))
  (add-to-list 'package-archives '("nongnu" . "https://elpa.nongnu.org/nongnu/"))

  ;; disable automatic loading of packages at startup
  (setq package-enable-at-startup nil)
  (package-initialize)
#+END_SRC

** =use-package=

As of Emacs 29, =use-package= is built-in.

#+BEGIN_SRC emacs-lisp
  (require 'use-package)
#+END_SRC


** =auto-compile=

To speed things up a bit. Make sure to use the newest version to compile as well.

#+BEGIN_SRC emacs-lisp
    (use-package auto-compile
      :ensure t
      :defer t
      :init (setq load-prefer-newer t)
      :config
      (auto-compile-on-load-mode)
      (auto-compile-on-save-mode))

#+END_SRC

* Defaults

Inspired by [[https://idiomdrottning.org/bad-emacs-defaults][Bad Emacs Defaults]].

#+BEGIN_SRC emacs-lisp
  ; Backups and autosave
  (make-directory "~/.emacs.d/autosave/" t)
  (make-directory "~/.emacs.d/backup/" t)
  (setq auto-save-file-name-transforms '((".*" "~/.emacs.d/autosave/" t)))
  (setq backup-directory-alist '((".*" . "~/.emacs.d/backup/")))

  (setq backup-by-copying t)

  ; C-h as backspace
  (global-set-key [(control h)] 'delete-backward-char)
  (keyboard-translate ?\C-h ?\C-?)

  ; Quality of life stuff
  (setq require-final-newline t)
  (setq frame-inhibit-implied-resize t)
  (setq pixel-scroll-precision-mode t)
  (setq show-trailing-whitespace t)
#+END_SRC

* Tramp

- Use Windows 10's OpenSSH implementation which doesn't support ~ControlMaster~. First we disable ~ControlMaster~ and then modify ~ssh~ login arguments to include ~-tt~ as there is an issue with allocating tty on the remote end.

#+BEGIN_SRC emacs-lisp
  (use-package tramp
    :config
    (when (string= system-type "windows-nt")
      (setq tramp-use-ssh-controlmaster-options nil)
      (setq tramp-methods (cl-remove-if (lambda (method) (string= "ssh" (car method))) tramp-methods))
      (add-to-list 'tramp-methods '("ssh"
                                    (tramp-login-program "ssh")
                                    (tramp-login-args
                                     (("-l" "%u") ("-p" "%p") ("%c") ("-e" "none") ("-t" "-t")
                                      ("%h")))
                                    (tramp-async-args (("-q")))
                                    (tramp-remote-shell "/bin/sh")
                                    (tramp-remote-shell-login ("-l"))
                                    (tramp-remote-shell-args ("-c"))))))
#+END_SRC

* Look and Feel
** Startup

#+BEGIN_SRC emacs-lisp
  (setq inhibit-startup-screen 1)
  (setq initial-scratch-message ";; scratch\n\n")
  (tool-bar-mode -1)
  (menu-bar-mode -1)
  (scroll-bar-mode -1)
  (setq column-number-mode 1)
  (setq display-line-numbers-type 'relative)
  (global-display-line-numbers-mode 1)
#+END_SRC

** Scrolling

#+BEGIN_SRC emacs-lisp
  (setq scroll-margin 6
        scroll-conservatively 9999
        scroll-step 1)
#+END_SRC

** Parentheses

#+BEGIN_SRC emacs-lisp
  (setq show-paren-delay 0)
  (add-hook 'prog-mode-hook 'show-paren-mode)
#+END_SRC

** Color, Fonts, and daemon mode

Loading theme for frames as I use daemonised emacs.

#+BEGIN_SRC emacs-lisp
  (use-package catppuccin-theme
               :ensure t)

  ;; load fonts and theme after daemon is loaded
  (defun my-load-theme (frame)
    "Load frame-specific options.  Takes frame as FRAME."
    (select-frame frame)

    (let ((myfont "nil"))
      (cond
       ((member "JetBrains Mono"    (font-family-list)) (setq myfont "JetBrains Mono"))
       ((member "Consolas"          (font-family-list)) (setq myfont "Consolas"))
       ((member "monospace"         (font-family-list)) (setq myfont "monospace")))
      (set-frame-font (concat myfont "-14")))

    (setq catppuccin-flavor 'latte)
    (load-theme 'gruber-darker t))

  (if (daemonp)
      (add-hook 'after-make-frame-functions #'my-load-theme)
    (my-load-theme (selected-frame)))
#+END_SRC

* Getting help with =which-key=

#+BEGIN_SRC emacs-lisp
    (use-package which-key
      :ensure t
      :hook (after-init . which-key-mode)
      :init
      (setq which-key-popup-type 'side-window)
      :config
      (which-key-setup-side-window-bottom))
#+END_SRC

* Code editing

** Tabs vs Spaces: the age-old debate

#+BEGIN_SRC emacs-lisp
  (setq-default indent-tabs-mode nil)
  (setq c-basic-offset 4)
  (add-hook 'before-save-hook #'delete-trailing-whitespace)
#+END_SRC

** Undo-tree

#+BEGIN_SRC emacs-lisp
  (use-package undo-tree
    :ensure t
    :hook (after-init . global-undo-tree-mode)
    :config
    (setq undo-tree-auto-save-history nil) ; use undo-tree-save/load-history manually instead
    (setq undo-tree-history-directory-alist '(("~/.emacs.d/.cache")))) ; don't litter
#+END_SRC

** =crux= utilities

#+BEGIN_SRC emacs-lisp
  (use-package crux
    :ensure t
    :bind
    (("C-k" . crux-smart-kill-line)
     ("C-o" . crux-smart-open-line)))
#+END_SRC

** =multiple-cursors=

#+BEGIN_SRC emacs-lisp
  (use-package multiple-cursors
    :ensure t
    :bind
    (("C-S-c C-S-c" . mc/edit-lines)
     ("C->" . mc/mark-next-like-this)
     ("C-<" . mc/mark-previous-like-this)
     ("C-c C-<" . mc/mark-all-like-this)
     ("C-\"" . mc/skip-to-next-like-this)
     ("C-:" . mc/skip-to-previous-like-this)))
#+END_SRC

* Code lifecycle and project management

This is where compilation and running comes into play.

#+begin_src emacs-lisp
  (setq compilation-scroll-output t)
  (setq compilation-always-kill t)
  (setq ansi-color-for-compilation-mode t)
#+end_src

I've previously used =projectile=, but found out =project.el= is more than capable. =C-x p= is all I need.

#+begin_src emacs-lisp
  ;; Sometimes things are not version-controlled
  (defun my/find-project-root (dir)
    (let ((override (locate-dominating-file dir ".project.el")))
      (if override
          (cons 'vc override)
        nil)))

  (use-package project
    :bind-keymap ("C-c p" . project-prefix-map)
    :bind (:map project-prefix-map ("m" . #'magit-project-status))
    :init (setq project-vc-extra-root-markers '(".project.el")))
#+end_src

* Completions and search

From =helm= to =ivy= and =counsel= all the way to =fido-vertical-mode= (builtin as of Emacs 29). I've found I just don't use all the fanciness of the bigger packages. So, nothing to see here!

#+BEGIN_SRC emacs-lisp
  (use-package icomplete
    :init
    (fido-vertical-mode t))
#+END_SRC

I use =eglot= (builtin as of Emacs 29) for auto-completion which has =completion-at-point= backend, invoked by =C-M-i=. I invoke it manually, so no configuration is needed. =corfu= is also nice for the autocompletion interface.

* Magit

#+BEGIN_SRC emacs-lisp
  (use-package magit
    :ensure t
    :defer t)
#+END_SRC

* Movement, buffers, and windows

#+BEGIN_SRC emacs-lisp
  (global-set-key (kbd "C-x k") 'kill-this-buffer)
#+END_SRC

#+BEGIN_SRC emacs-lisp
  (use-package avy
    :ensure t
    :defer t
    :bind (("C-." . avy-goto-word-1)
           ("C-," . avy-goto-line)))
#+END_SRC

=ace-window= helps move around windows. incidentally, it makes using windows more pleasant and not something to be avoided

#+BEGIN_SRC emacs-lisp
  (use-package ace-window
    :ensure t
    :bind ("C-c o" . ace-window)
    :init (setq aw-keys '(?h ?t ?n ?s ?a ?o ?e ?u)))
#+END_SRC

I've found [[https://www.gnu.org/software/emacs/manual/html_node/emacs/Registers.html][registers]] are pretty amazing to move around

#+BEGIN_SRC emacs-lisp
  (setq register-preview-delay 0)
#+END_SRC

* Backups

#+BEGIN_SRC emacs-lisp
  (setq backup-directory-alist `((".*" . ,temporary-file-directory)))
  (setq auto-save-file-name-transforms `((".*" ,temporary-file-directory t)))
#+END_SRC

* Life in =Org-mode=

Org-mode has [[https://orgmode.org/manual/][excellent documentation]] which includes [[https://orgmode.org/manual/Publishing.html#Publishing][publishing]].

#+BEGIN_SRC emacs-lisp
  (defun my/org-archive-done ()
    (interactive)
    (org-map-entries
     (lambda ()
       (org-archive-subtree)
       (setq org-map-continue-from (org-element-property :begin (org-element-at-point))))
     "/+DONE" 'file))

  (use-package org
    :ensure nil
    :mode ("\\.org\\'" . org-mode)
    :init
    (setq org-src-fontify-natively t) ; syntax highlighting in source blocks
    (setq org-src-tab-acts-natively t) ; make tab act natively in source blocks
    (setq org-src-window-setup 'current-window) ; editing source block edits in current window

                                          ; Babel
    (org-babel-do-load-languages 'org-babel-load-languages '((python . t)))

                                          ; Agenda
    (setq org-log-done 'time) ; record when a todo was done
    (setq org-agenda-files (list (file-truename (file-name-as-directory "~/notes")))) ; todo index file
    (setq org-agenda-dim-blocked-tasks nil)
    (setq org-enforce-todo-dependencies t)
    (setq org-enforce-todo-checkbox-dependencies t)
    (setq org-agenda-sorting-strategy '((agenda todo-state-up
                                                priority-down
                                                time-up
                                                habit-down
                                                category-down)))
    (setq org-archive-location "todo_archive::")
    (setq org-agenda-skip-deadline-prewarning-if-scheduled 'pre-scheduled)
    (setq org-agenda-prefix-format '((agenda . "%-6e")
                                     (todo . "%-6e")
                                     (tags . "")
                                     (search . "")))
    (setq org-highest-priority ?A)
    (setq org-lowest-priority ?D)
    (setq org-default-priority ?C)

                                          ; Agenda view
    (setq org-agenda-custom-commands
          '(("p" "TODOs and Projects"
             ((tags-todo "PRIORITY=\"A\"&SCHEDULED=\"<today>\""
                         ((org-agenda-overriding-header "Today's Priority")))
              (tags-todo "+errand&SCHEDULED>=\"<today>\"&SCHEDULED<=\"<+1d>\""
                         ((org-agenda-overriding-header "Tomorrow's Errands")))
              (agenda ""
                      ((org-agenda-span 'week)))
              (todo "ACTIVE"
                    ((org-agenda-overriding-header "Active Projects")))))))

                                          ; publishing website
    (setq org-html-htmlize-output-type 'css))
#+END_SRC

** Org-roam for ultra-note-taking

#+BEGIN_SRC emacs-lisp
  (use-package org-roam
    :ensure t
    :init
    (setq org-roam-v2-ack t)
    (setq org-roam-directory (file-truename "~/notes"))
                                          ; use id instead of title for filename
    (setq org-roam-capture-templates
          '(("d" "default" plain "%?"
             :if-new (file+head "%<%Y%m%d%H%M%S>.org" "#+title: ${title}
  ")
             :unnarrowed t)))
    (setq org-roam-dailies-directory "daily/")
    (setq org-roam-dailies-capture-templates
          '(("d" "default" entry "* %?"
             :if-new (file+head "%<%Y-%m-%d>.org" "#+title: Daily %<%Y-%m-%d>
  "))))
    :config
    (org-roam-setup))
#+END_SRC

* Writing

#+BEGIN_SRC emacs-lisp
  (use-package writeroom-mode
    :ensure t
    :init
    (add-hook 'writeroom-mode-hook #'visual-line-mode))
#+END_SRC

* Language-specific modes and configs

** Godot

I've started playing around with Godot which is a lot of fun. They have official gdscript mode for emacs which is a bit surprising.

#+BEGIN_SRC emacs-lisp
  (use-package gdscript-mode
    :defer t
    :hook (gdscript-mode . eglot-ensure)
    :mode
    (("\\.gd$" . gdscript-mode)
     ("\\.tscn$" . gdscript-mode))
    :init
    (when (string= system-type "windows-nt")
      (setq gdscript-godot-executable (expand-file-name "~/pkgs/Godot/Godot_v4.0.3-stable_win64.exe"))))
#+END_SRC

** Eglot

#+begin_src emacs-lisp
  (use-package eglot
    :config
    (add-to-list 'eglot-server-programs
                 '(kotlin-mode . ("kotlin-language-server.bat"))
                 '(go-ts-mode . ("gopls"))))
#+end_src

** Zig

#+begin_src emacs-lisp
  (use-package zig-mode
    :ensure t)
#+end_src

* Web development

#+begin_src emacs-lisp
  (use-package web-mode
    :ensure t)
;    :config
;    (add-to-list 'auto-mode-alist '("\\.html?\\'" . web-mode)))
#+end_src
